---
layout: page
title: About
subtitle:
permalink: /about/
---

<p class="about-text">
<span class="fa fa-address-card about-icon"></span>
My name is Paul.
</p>
<p class="about-text">
<span class="fa fa-globe-europe about-icon"></span>
I live in Scotland, in the UK.
</p>
<p class="about-text">
<span class="fa fa-heart about-icon"></span>
I am a part-time developer, full time techie and tinkerer. 
</p>
<p class="about-text">
<span class="fa fa-briefcase about-icon"></span>
In my day job I look after large enterprise applications and the infrastructure around them..
</p>
<p class="about-text">
<span class="fa fa-paper-plane about-icon"></span>
Feel free to <a href="mailto:paul@sneddon.co?subject=Hello from sneddon.co">email me</a>. I’ll get back to you as soon as I can.
</p>
