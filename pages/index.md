---
layout: page
title: Welcome
subtitle:
permalink: /index.html
---
Hello and welcome.  
{: style="text-align: center;"}

Please feel free to look around.
{: style="text-align: center;"}
<div class="list-filters">
    <a href="/posts" class="list-filter">All</a>
    <a href="/posts/popular" class="list-filter">Popular</a>
    <a href="/posts/personal" class="list-filter">Personal</a>
    <a href="/posts/technical" class="list-filter">Technical</a>
    <a href="/tags" class="list-filter">Index</a>
  </div>
