---
layout: page
title: Tokens
subtitle: Support FAQ
permalink: /tokens/support-faq/
---
## I've forgotten my in-app passcode, how do I reset it?

You can't. The passcode is stored securely in the keychain of the local device (it is not synced with iCloud). There is no way for an app to easily verify who you are, since it does not know any of your personal details, and therefore no practical or secure way to perform a reset.

## Can I recover deleted tokens?

The short answer is no, since the tokens are only stored directly on the iCloud keychain. Once they are deleted, they are gone.  

If you have exported a backup, then you can use this to recover any deleted tokens (providing you remember the password you used to encrypt the back).  You can also create a backup of your tokens by saving the token QR code (you can find this in the manage token screen).  Ideally these should be printed and stored in a secure physical location, similar to how you would manage your account recover codes.

## When I import a backup will it overwrite my existing token?

No, when you import a backup it will merge with your existing tokens. Each token has a unique identifier attached to it so you don't need to worry about duplicates getting created.

