---
layout: page
title: TextArt
subtitle:
permalink: /textart/
---
A simple app that converts your photos into text
{: style="text-align: center;"}
<br>
![TextArt]({{ site.baseurl }}/assets/img/TextArt.png)  
{: style="text-align: center;"}
<br>
[![App Store]({{ site.baseurl }}/assets/img/app_store.png)](https://itunes.apple.com/app/id326500591)
{: style="text-align: center;"}  
<br>
For support email [support@sneddon.co](mailto:support@sneddon.co)
{: style="text-align: center;"}

