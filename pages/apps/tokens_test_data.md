---
layout: page
title: Tokens
subtitle: Test Data
permalink: /tokens/test-data/
---
The test tokens below can be imported by clicking on the hyperlinks, they should open in the Tokens App if you have it installed.

[Test Token 1](otpauth://hotp/test1@test.com?algorithm=SHA1&digits=6&issuer=Example%201&secret=JBSWY3DPEHPK3PXX&counter=144)
<br>
[Test Token 2](otpauth://totp/test2@test.com?algorithm=SHA1&digits=6&issuer=Short%20Timer&secret=JBSWY3DPEHPK3PX3&period=10)
<br>
[Test Token 3](otpauth://totp/test3@test.com?algorithm=SHA1&digits=6&issuer=Microsoft&secret=JBSWY3DPEHPK3PXA&period=30)
<br>
[Test Token 4](otpauth://totp/test4@test.com?algorithm=SHA1&digits=6&issuer=Evernote&secret=JBSWY3DPEHPK3PXB&period=60)
<br>
[Test Token 5](otpauth://totp/test5@test.com?algorithm=SHA1&digits=6&issuer=Server&secret=JBSWY3DPEHPK3PYA&period=30)

The following QR image can be scanned into the Tokens app (press the + button in the top left of the main screen)


<img src="/img/QR.png" alt="QR" width="256" height="256" />

