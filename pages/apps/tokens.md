---
layout: page
title: Tokens
subtitle:
permalink: /tokens/
---

Tokens is a 2-factor authentication application which generates and manages one time passwords for account providers such as Google, Microsoft, Dropbox, Evernote, Amazon, and Facebook, or any account provider who are compatible with Google Authenticator or 1Password 2FA.


## Main Features:
^
* iCloud Keychain syncing.
* Import tokens by scanning QR codes.
* Support both time and counter based tokens.
* Creation of encrypted backups.
* Password and TouchID support.
* Supports iPhone, iPad, and iPad Pro
^

<br>
![Tokens]({{ site.baseurl }}/assets/img/Tokens.png)
{: style="text-align: center;"}
<br>
[![App Store]({{ site.baseurl }}/assets/img/app_store.png)](https://itunes.apple.com/app/id1086924895)
{: style="text-align: center;"}
<br>
For support email [support@sneddon.co](mailto:support@sneddon.co).  
{: style="text-align: center;"}
<!-- A [support FAQ](/tokens/support-faq) and [detailed features overview](/tokens/features) are also available. -->
