---
layout: post
title: Linux, Where Are My Menu Icons?
subtitle:
tags: [Linux]
categories: [Technical, Popular]
excerpt_separator: <!--more-->
---

I use Linux every day, and have done so for over a decade, but for nearly all of that time I've only used the command line (vis SSH).

<!--more-->

I recently started studying for the Cisco CCNA quaification, a great application to help with this is [GNS3](https://en.wikipedia.org/wiki/Graphical_Network_Simulator-3).  While you can run GNS3 on a Mac or Windows, for various reasons which I won't go into just now, it runs much better on Linux.

GNS makes use of menu icons to convey state, for example a right click content menu can show red or green icons to signify if a connection is in use or not.  Without this you have to remember or guess.
<!-- <center><img class="border-light aligncenter" src="{{ site.baseurl }}/assets/img/GNS3.jpg" alt="GNS3" /></center> -->

I wanted a Linux distribution which was well supported with a minimal but functional desktop environment.  I didn't want the bloat of lots of additional applications which I couldn't uninstall.  I tried all the main candidates consisting of various flavours of Debian or Ubuntu, fused with either the Gnome, Mate, Cinnamon, LXDE, or Xfce desktops.

To cut a long story short, I settled first on a distribution called elementary OS (which is based on Ubuntu), but I noticed that icons were not showing on the GNS3 menus (or even in other applications).  I assumed this was a choice by the designers (to make it look like the Mac OS X desktop has no menu icons either), so I went in search of another distribution / desktop environment.  I settled then on Debian with Gnome....however this turned out to have the same issue with the missing menu icons.

Now I know that with Linux, the GUI world is made up of such things as display managers, window managers, and desktop managers.  And I know that GNS3 is an application based on the QT4 framework, which I understand is some sort of GUI widget framework.  And I also know there is something else similar to QT4 called GTK.  So with this all swirling around my head I went-a-goggling.

I spent a fair amount of time searching for an explanation or solution. A first I could find no direct hits (the words menu, icon, and Linux are so common due to Linux spanning back decades with it's multitude of distributions and desktops).  I then hit upon a few potential solutions. ...but none of them solved my issue.  I was about to give up, but I then stumbled across the following solution.

    gconftool-2 --type boolean --set /desktop/gnome/interface/buttons_have_icons true
    gconftool-2 --type boolean --set /desktop/gnome/interface/menus_have_icons true

It turns out, it was all the fault of something called [GTK 3.10](https://igurublog.wordpress.com/2014/03/22/gtk-3-10-drops-menu-icons-and-mnemonics/).