---
layout: post
title: Connection Timeout When Trying to SSH From a Mac to Linux
subtitle:
tags: [Linux, macOS]
categories: [Technical]
---

Recently I had to SSH from Mac OS X on my MacBook Pro running Yosemite (I also confirmed the same behaviour on Mavericks) to a host running Oracle Enterprise Linux 5.7. The ssh command would simply freeze, then time out with the following error a short while later.

<!--more-->

    Read from socket failed: Operation timed out

Turing on verbose mode (x2) resulted in the following clues just before hanging pending the timeout.

    debug2: mac_setup: found hmac-md5
    debug1: kex: server->client aes128-ctr hmac-md5 none
    debug2: mac_setup: found hmac-md5
    debug1: kex: client->server aes128-ctr hmac-md5 none
    debug1: SSH2_MSG_KEX_DH_GEX_REQUEST(1024>1024>8192) sent
    debug1: expecting SSH2_MSG_KEX_DH_GEX_GROUP
    Read from socket failed: Operation timed out

So it looks like the server stops responding when the client is negotiating which HMAC to use. I’m not clued up enough on the inner workings of OpenSSH to figure out anymore than that. A google search did not turn up anything concrete matching the same issue with a definitive solution, although I did find out enough to know which configuration options are used for the HMAC, and in which config files

The global client SSH config file is /etc/ssh_config, on Mac OS X every single value is commented out, so defaults are used. The option and values we are interested in is

    MACs hmac-md5,hmac-sha1,umac-64@openssh.com,hmac-ripemd160,hmac-sha1-96,hmac-md5-96,hmac-sha2-256,hmac-sha2-256-96,hmac-sha2-512,hmac-sha2-512-96`

However if you just copy these values into a user specific config file ~/.ssh/config, or even alther the global config file, then you get the this error.

    ~/.ssh/config line 5: Bad protocol 2 host key algorithms 'ecdsa-sha2-nistp256-cert-v01@openssh.com, ecdsa-sha2-nistp384-cert-v01@openssh.com,ecdsa-sha2-nistp521-cert-v01@openssh.com,ssh-rsa-cert-v01@openssh.com,ssh-dss-cert-v01@openssh.com,ssh-rsa-cert-v00@openssh.com,ssh-dss-cert-v00@openssh.com,ecdsa-sha2-nistp256,ecdsa-sha2-nistp384,ecdsa-sha2-nistp521,ssh-rsa,ssh-dss'.
    
Unfortunately, the error message does not indicate which values it is not happy with, so the process of removing them all and adding them one by one is required (or maybe there is a command you can run which will list the supported HMACs?). Once the process is complete, we are left with the following…

    MACs hmac-sha1,umac-64@openssh.com,hmac-ripemd160,hmac-sha1-96,hmac-md5-96,hmac-sha2-256

Once the MACs option is specified with these values in either the global config, or in the user config, then the problem is solved and SSH will work as expected. I’m not sure if this is intended behaviour or a bug with the negotiation, but it would have been a lot easier if SSH had been clearer with what the issue was