---
layout: post
title: Prevent Accidentally Activating Hot Corners in MacOS
subtitle:
categories: [Technical, Popular]
tags: [macOS]
---

I find Hot Corners an essential MacOS feature, especially when using apps in full-screen mode or screen sharing.  One downside of the feature is that it’s easy to accidentally activate. A way to prevent this, which isn’t at all obvious, is to require a modifier key as well as moving your mouse into the corner.

<!--more-->

<center><img class="border-light aligncenter" src="{{ site.baseurl }}/assets/img/HotCorners.jpg" alt="Hot Corners" /></center>

When configuring a hot corner, hold down Shift **⇧**, Control **⌃**, Option **⌥**, or Command **⌘** when choosing the function the corner activates. Each corner can be configured independently to use a different modifier key.

